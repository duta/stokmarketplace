Rest Documentation :

There's no login when using rest api, instead you must make sure that the client is whitelisted in web.config. Search for these lines :

    <add key="AllowedAPISingleIPs" value="localhost,127.0.0.1" />
    <add key="AllowedAPIMaskedIPs" value="192.168.2.0;255.255.255.0,192.168.56.0;255.255.255.0" />

GET protocols :
- <host>/api/StoreAPI/&lt;userid> (&lt;userid> is the same as the one in user.id)  

This will fetch all Stores saved under said user and print the result as a json array with these structure :  
{  
    string StoreUrl;  
    string Marketplace;  
    string Username;  
    string Password; (this column will show up empty for now)  
    string Storename;  
}  

- <host>/api/ItemAPI/&lt;userid> (&lt;userid> is the same as the one in user.id)  

This will fetch all Items saved under said user and print the result as a json array with these structure :  
{  
	string IdItem; (same as material.id)  
	string StoreUrl;  
	string ItemUrl;  
	string ItemName; (taken from material.nama)  
}  

POST protocols :
- <host>/api/StoreAPI/

{  
	string Marketplace;  
	string StoreUrl;  
	string Username;  
	string Password; (plain text for now)  
	string UserId; (same as user.id)  
}  
Creates a new store under said user  

- <host>/api/ItemAPI/

{  
	string IdItem; (same as material.id)  
	string StoreUrl;  
	string ItemUrl;  
}  
Register a new url for an item (make sure the storeurl is the one the item is registered in)  

PUT protocol :
- <host>/api/ItemAPI/

{  
	string Id; (same as material.id)  
	string Stock;  
}  
Update the stock for said item  

DELETE protocol :
- <host>/api/StoreAPI/?url=&lt;storeurl> (make sure the storeurl is url-encoded)
- <host>/api/ItemAPI/&lt;itemid> (&lt;itemid> is the same as the one in material.id)
