﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace StockBarang.Controllers
{
    [Attributes.Controller.FilterIP(
        ConfigurationKeyAllowedSingleIPs = "AllowedAPISingleIPs",
        ConfigurationKeyAllowedMaskedIPs = "AllowedAPIMaskedIPs")]
    public class ItemAPIController : ApiController
    {
        // GET: api/StoreAPI
        public IEnumerable<Helper.Item> Get(string id)
        {
            var stores = Helper.MySqlHelper.Getstock_items(id, -1);
            var res = new Helper.Item[stores.Count];
            for (int i = 0; i < stores.Count; i++)
            {
                res[i] = stores[i];
            }
            return res;
        }

        // POST: api/StoreAPI
        public string Post([FromBody]Helper.Item c)
        {
            try
            {
                Helper.MySqlHelper.InsertItem(c);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "Success";
        }

        // Put: api/StoreAPI
        public string Put([FromBody]ItemPut c)
        {
            try
            {
                var model = Helper.MySqlHelper.GetItem(c.Id);
                var store = Helper.MySqlHelper.GetCompleteStore(model.StoreUrl);
                switch (store.Marketplace)
                {
                    case "Tokopedia":
                        Helper.TokopediaHelper.ChangeStock(model.ItemUrl, store, c.Stock);
                        break;
                    case "Bukalapak":
                        Helper.BukalapakHelper.ChangeStock(model.ItemUrl, store, c.Stock);
                        break;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "Success";
        }

        // DELETE: api/StoreAPI/5
        public void Delete(string id)
        {
            Helper.MySqlHelper.DeleteItem(id);
        }
    }

    public class ItemPut
    {
        public string Id { get; set; }
        public string Stock { get; set; }
    }
}
