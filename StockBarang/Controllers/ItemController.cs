﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using StockBarang.Helper;
using StockBarang.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StockBarang.Controllers
{
    [Authorize]
    public class ItemController : Controller
    {
        // GET: Item
        public ActionResult List(int page = 1)
        {
            var user = Helper.MySqlHelper.GetUserByName(User.Identity.Name);
            var model = new ItemListModel
            {
                Items = Helper.MySqlHelper.Getstock_items(user.Id, page)
            };
            Helper.PagingHelper.FillPaging(model, page, Helper.MySqlHelper.GetTotalstock_items(user.Id));
            return View(model);
        }

        // GET: Store/Create
        public ActionResult Create()
        {
            var model = new ItemCreateModel
            {
                StoreList = Helper.MySqlHelper.GetStores(MySqlHelper.GetUserByName(User.Identity.Name).Id),
                MaterialList = Helper.MySqlHelper.GetMaterials()
            };
            return View(model);
        }

        // POST: Store/Create
        [HttpPost]
        public ActionResult Create(ItemCreateModel model)
        {
            try
            {
                MySqlHelper.InsertItem(new Item() { IdItem = model.ID, ItemUrl = model.ItemUrl, StoreUrl = model.Store });
                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                var modl = new ItemCreateModel
                {
                    StoreList = Helper.MySqlHelper.GetStores(MySqlHelper.GetUserByName(User.Identity.Name).Id),
                    MaterialList = Helper.MySqlHelper.GetMaterials()
                };
                return View(modl);
            }
        }

        // GET: Store/Create
        public ActionResult Stock(string id)
        {
            var model = new ItemStockModel
            {
                Item = Helper.MySqlHelper.GetItem(id)
            };
            return View(model);
        }

        // POST: Store/Create
        [HttpPost]
        public ActionResult Stock(ItemStockModel model)
        {
            try
            {
                var store = MySqlHelper.GetCompleteStore(model.Item.StoreUrl);
                switch (store.Marketplace)
                {
                    case "Tokopedia":
                        TokopediaHelper.ChangeStock(model.Item.ItemUrl, store, model.stock);
                        break;
                    case "Bukalapak":
                        BukalapakHelper.ChangeStock(model.Item.ItemUrl, store, model.stock);
                        break;
                }
                return RedirectToAction("List");
            }
            catch (Exception ex)
            {
                var modl = new ItemStockModel
                {
                    Item = Helper.MySqlHelper.GetItem(model.Item.IdItem)
                };
                return View(modl);
            }
        }
    }
}