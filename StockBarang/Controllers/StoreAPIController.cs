﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace StockBarang.Controllers
{
    [Attributes.Controller.FilterIP(
        ConfigurationKeyAllowedSingleIPs = "AllowedAPISingleIPs",
        ConfigurationKeyAllowedMaskedIPs = "AllowedAPIMaskedIPs")]
    public class StoreAPIController : ApiController
    {
        // GET: api/StoreAPI
        public IEnumerable<Helper.Store> Get(string id)
        {
            var stores = Helper.MySqlHelper.GetStores(id, -1);
            var res = new Helper.Store[stores.Count];
            for (int i = 0; i < stores.Count; i++)
            {
                res[i] = stores[i];
            }
            return res;
        }

        // POST: api/StoreAPI
        public string Post([FromBody]Models.StoreCreateModel c)
        {
            try
            {
                switch (c.Marketplace)
                {
                    case "Tokopedia":
                        Helper.MySqlHelper.InsertStore(c, c.UserId,
                            Helper.TokopediaHelper.FetchStoreName(c.StoreUrl, c.Username, c.Password));
                        break;
                    case "Bukalapak":
                        Helper.MySqlHelper.InsertStore(c, c.UserId,
                            Helper.BukalapakHelper.FetchStoreName(c.StoreUrl, c.Username, c.Password));
                        break;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "Success";
        }

        // DELETE: api/StoreAPI/5
        public void Delete(string url)
        {
            Helper.MySqlHelper.DeleteStore(url);
        }
    }
}
