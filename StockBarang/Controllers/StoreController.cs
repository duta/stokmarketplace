﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using StockBarang.Helper;
using StockBarang.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StockBarang.Controllers
{
    [Authorize]
    public class StoreController : Controller
    {
        // GET: Store
        public ActionResult List(int page = 1)
        {
            var user = Helper.MySqlHelper.GetUserByName(User.Identity.Name);
            var model = new StoreListModel
            {
                Stores = Helper.MySqlHelper.GetStores(user.Id, page)
            };
            Helper.PagingHelper.FillPaging(model, page, Helper.MySqlHelper.GetTotalStores(user.Id), 15);
            return View(model);
        }

        // GET: Store/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Store/Create
        [HttpPost]
        public ActionResult Create(StoreCreateModel model)
        {
            var user = Helper.MySqlHelper.GetUserByName(User.Identity.Name);
            //try
            //{
            switch (model.Marketplace)
                {
                    case "Tokopedia":
                        MySqlHelper.InsertStore(model, user.Id,
                            TokopediaHelper.FetchStoreName(model.StoreUrl, model.Username, model.Password));
                        break;
                    case "Bukalapak":
                        MySqlHelper.InsertStore(model, user.Id,
                            BukalapakHelper.FetchStoreName(model.StoreUrl, model.Username, model.Password));
                        break;
                }
                return RedirectToAction("List");
            //}
            //catch (Exception ex)
            //{
            //    return View();
            //}
        }

        public ActionResult Delete(string url)
        {
            Helper.MySqlHelper.DeleteStore(url);
            return RedirectToAction("List");
        }
    }
}
