﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockBarang.Helper
{
    public static class BukalapakHelper
    {
        public static string FetchStoreName(string storeurl, string username, string password)
        {
            using (ChromeDriver driver = new ChromeDriver())
            {
                driver.Url = "https://www.bukalapak.com/login";
                driver.Navigate();
                driver.FindElement(By.Id("user_session_username")).SendKeys(username);
                driver.FindElement(By.Id("user_session_password")).SendKeys(password);
                driver.FindElement(By.XPath("//button[@name='commit']")).Click();
                driver.FindElement(By.ClassName("c-list-ui__link--username")).GetAttribute("innerText");
                driver.Url = storeurl;
                driver.Navigate();
                var storename = driver.FindElement(By.ClassName("user__name")).Text;
                driver.Url = "https://www.bukalapak.com/logout";
                driver.Navigate();
                driver.Close();
                return storename;
            }
        }

        public static void ChangeStock(string url, Store store, string stock)
        {
            using (ChromeDriver driver = new ChromeDriver())
            {
                driver.Url = "https://www.bukalapak.com/login";
                driver.Navigate();
                System.IO.File.AppendAllText(@"C:\Users\vmptdmc\Documents\bllog\"+ DateTime.Now.Minute + DateTime.Now.Second + ".txt", driver.Url+ driver.PageSource);
                driver.FindElement(By.Id("user_session_username")).SendKeys(store.Username);
                driver.FindElement(By.Id("user_session_password")).SendKeys(store.Password);
                driver.FindElement(By.XPath("//button[@name='commit']")).Click();
                driver.FindElement(By.ClassName("c-list-ui__link--username")).GetAttribute("innerText");
                driver.Url = url;
                driver.Navigate();
                driver.ExecuteScript("document.getElementById('product_product_sku_stock').setAttribute('value', '" + stock + "')");
                driver.ExecuteScript("$('.btn--commit').click();");
                driver.Url = "https://www.bukalapak.com/logout"; 
                driver.Navigate();
                driver.Close();
            }
        }
    }
}