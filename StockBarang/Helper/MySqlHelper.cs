﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Web.Configuration;
using System;

namespace StockBarang.Helper
{
    public class MySqlHelper
    {
        private static string connString = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public static User GetUserByName(string username)
        {
            User res = new User();
            MySqlConnection _ps = new MySqlConnection(connString);
            _ps.Open();
            MySqlCommand cmd = new MySqlCommand("SELECT u.id 'id', u.username 'name', u.password 'pass', r.fullname 'role' FROM user as u left join role as r on u.role=r.id where username = \"" + username+"\";", _ps);

            var reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                res.Id = reader["id"].ToString();
                res.Name = reader["name"].ToString();
                res.Pass_hash = reader["pass"].ToString();
                res.Role = reader["role"].ToString();
            }
            reader.Close();
            _ps.Close();
            return res;
        }

        public static List<Material>  GetMaterials()
        {
            List<Material> res = new List<Material>();
            MySqlConnection _ps = new MySqlConnection(connString);
            _ps.Open();
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM material", _ps);

            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Material c = new Material();
                c.Id = reader["id"].ToString();
                c.Nama = reader["nama"].ToString();
                res.Add(c);
            }
            reader.Close();
            _ps.Close();
            return res;
        }

        public static List<Store> GetStores(string userid, int page = 1)
        {
            List<Store> res = new List<Store>();
            MySqlConnection _ps = new MySqlConnection(connString);
            _ps.Open();
            var paging = (page==-1 ? "" : " limit " + ((page - 1) * 15) + ", " + 15);
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM `stock_store` where user_id = " + userid + " ORDER BY marketplace desc" + paging, _ps);

            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Store c = new Store();
                c.StoreUrl = reader["store_url"].ToString();
                c.Marketplace = reader["marketplace"].ToString();
                c.Username = reader["username"].ToString();
                c.Storename = reader["store_name"].ToString();
                res.Add(c);
            }
            reader.Close();
            _ps.Close();
            return res;
        }

        internal static Store GetCompleteStore(string url)
        {
            Store c = new Store();
            MySqlConnection _ps = new MySqlConnection(connString);
            _ps.Open();
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM `stock_store` where store_url = '" + url + "'", _ps);

            var reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                c.StoreUrl = reader["store_url"].ToString();
                c.Marketplace = reader["marketplace"].ToString();
                c.Username = reader["username"].ToString();
                c.Password = SimpleEncryptHelper.Decrypt(reader["password"].ToString(), reader["user_id"].ToString(), c.Username);
                c.Storename = reader["store_name"].ToString();
            }
            reader.Close();
            _ps.Close();
            return c;
        }

        public static int GetTotalStores(string userid)
        {
            MySqlConnection _ps = new MySqlConnection(connString);
            _ps.Open();

            MySqlCommand cmd = new MySqlCommand("select count(store_url) from stock_store where user_id = " + userid, _ps);

            int res = int.Parse(cmd.ExecuteScalar().ToString());

            _ps.Close();
            return res;
        }

        public static void InsertStore(Models.StoreCreateModel c, string userid, string storename)
        {
            MySqlConnection _ps = new MySqlConnection(connString);
            _ps.Open();
            var password = Helper.SimpleEncryptHelper.Encrypt(c.Password, userid, c.Username);
            MySqlCommand cmd = new MySqlCommand("INSERT INTO `stock_store`(`store_url`, `marketplace`, `username`, `password`, `store_name`, `user_id`) " +
                    " VALUES ('" + c.StoreUrl + "', '" + c.Marketplace + "', '" + c.Username + "', '" + password + "', '" + storename + "', " + userid + " )", _ps);
            cmd.ExecuteNonQuery();
            _ps.Close();
        }

        public static void DeleteStore(string url)
        {
            MySqlConnection _ps = new MySqlConnection(connString);
            _ps.Open();
            MySqlCommand cmd = new MySqlCommand("delete from stock_store where store_url = '" + url + "'", _ps);
            cmd.ExecuteNonQuery();
            _ps.Close();
        }

        public static Item GetItem(string itemid)
        {
            Item c = new Item();
            MySqlConnection _ps = new MySqlConnection(connString);
            _ps.Open();
            MySqlCommand cmd = new MySqlCommand("SELECT i.*, m.nama FROM stock_item as i left join material as m on i.iditem=m.nama where iditem='" + itemid + "' ", _ps);

            var reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                c.IdItem = reader["iditem"].ToString();
                c.StoreUrl = reader["store_url"].ToString();
                c.ItemUrl = reader["item_url"].ToString();
                c.ItemName = reader["nama"].ToString();
            }
            reader.Close();
            _ps.Close();
            return c;
        }

        public static List<Item> Getstock_items(string userid, int page = 1)
        {
            List<Item> res = new List<Item>();
            MySqlConnection _ps = new MySqlConnection(connString);
            _ps.Open();
            var paging = (page == -1 ? "" : " limit " + ((page - 1) * 15) + ", " + 15);
            MySqlCommand cmd = new MySqlCommand("SELECT i.*, m.nama 'nama' FROM stock_item as i left join material as m on i.iditem=m.id where store_url in (SELECT store_url FROM `stock_store` where user_id = " + userid + ")"+paging, _ps);

            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Item c = new Item();
                c.IdItem = reader["iditem"].ToString();
                c.StoreUrl = reader["store_url"].ToString();
                c.ItemUrl = reader["item_url"].ToString();
                c.ItemName = reader["nama"].ToString();
                res.Add(c);
            }
            reader.Close();
            _ps.Close();
            return res;
        }

        public static int GetTotalstock_items(string userid)
        {
            MySqlConnection _ps = new MySqlConnection(connString);
            _ps.Open();

            MySqlCommand cmd = new MySqlCommand("select count(iditem) from stock_item where store_url in (SELECT store_url FROM `stock_store` where user_id = " + userid + ")", _ps);

            int res = int.Parse(cmd.ExecuteScalar().ToString());

            _ps.Close();
            return res;
        }

        public static void InsertItem(Item c)
        {
            MySqlConnection _ps = new MySqlConnection(connString);
            _ps.Open();
            MySqlCommand cmd = new MySqlCommand("INSERT INTO `stock_item`(`iditem`, `store_url`, `item_url`) " +
                    " VALUES ('" + c.IdItem + "', '" + c.StoreUrl + "', '" + c.ItemUrl + "')", _ps);
            cmd.ExecuteNonQuery();
            _ps.Close();
        }

        public static void DeleteItem(string iditem)
        {
            MySqlConnection _ps = new MySqlConnection(connString);
            _ps.Open();
            MySqlCommand cmd = new MySqlCommand("delete from stock_item where iditem = '" + iditem + "'", _ps);
            cmd.ExecuteNonQuery();
            _ps.Close();
        }

        public static Attributes.Controller.IPList GetIPbyGroup(string group, bool allowed = true)
        {
            Attributes.Controller.IPList res = new Attributes.Controller.IPList();
            MySqlConnection _ps = new MySqlConnection(connString);
            _ps.Open();
            MySqlCommand cmd = new MySqlCommand("SELECT * FROM stock_ip WHERE grouped = '" + group+"' AND allowed = "+(allowed?"1":"0"), _ps);

            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                if (reader["mask"] == DBNull.Value)
                {
                    res.Add(reader["ip"].ToString());
                }
                else
                {
                    res.Add(reader["ip"].ToString(), reader["mask"].ToString());
                }
            }
            reader.Close();
            _ps.Close();
            return res;
        }
    }

    public class User
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Pass_hash { get; set; }
        public string Role { get; set; }
    }

    public class Item
    {
        public string IdItem { get; set; }
        public string StoreUrl { get; set; }
        public string ItemUrl { get; set; }
        public string ItemName { get; set; }
    }
    
    public class Material
    {
        public string Id { get; set; }
        public string Nama { get; set; }
    }

    public class Store
    {
        public string StoreUrl { get; set; }
        public string Marketplace { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Storename { get; set; }
    }
}