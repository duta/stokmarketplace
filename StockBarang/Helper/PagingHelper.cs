﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockBarang.Helper
{
    public static class PagingHelper
    {
        public static void FillPaging(IPageable model, int page, int totalPages, int itemPerPages = 10)
        {
            if (totalPages % itemPerPages != 0) totalPages += itemPerPages - (totalPages % itemPerPages);
            totalPages = totalPages / itemPerPages;
            List<PagingEntity> pList = new List<PagingEntity>
            {
                new PagingEntity() { PagingText = "1", disabled = false, active = (page == 1), href = (page == 1) ? "#" : "?page=1" }
            };
            if (page > 3) pList.Add(new PagingEntity() { PagingText = "...", disabled = true, active = false, href = "#" });
            if (page > 2) pList.Add(new PagingEntity() { PagingText = (page - 1).ToString(), disabled = false, active = false, href = "?page=" + (page - 1) });
            if (totalPages > 1 && page != 1 && page != totalPages) pList.Add(new PagingEntity() { PagingText = page.ToString(), disabled = false, active = true, href = "#" });
            if (page + 1 < totalPages) pList.Add(new PagingEntity() { PagingText = (page + 1).ToString(), disabled = false, active = false, href = "?page=" + (page + 1) });
            if (page + 2 < totalPages) pList.Add(new PagingEntity() { PagingText = "...", disabled = true, active = false, href = "#" });
            if (totalPages != 1) pList.Add(new PagingEntity() { PagingText = totalPages.ToString(), disabled = false, active = (page == totalPages), href = (page == totalPages) ? "#" : "?page=" + totalPages });
            model.Paging = pList;
        }
    }

    public interface IPageable
    {
        List<Helper.PagingEntity> Paging { get; set; }
    }

    public class PagingEntity
    {
        public string PagingText;
        public bool disabled;
        public bool active;
        public string href;
    }
}