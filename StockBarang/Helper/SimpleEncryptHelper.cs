﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace StockBarang.Helper
{
    public static class SimpleEncryptHelper
    {
        private static RijndaelManaged rm = new RijndaelManaged();
        private static UTF8Encoding encoder = new UTF8Encoding();

        private static string prepKeyVector(string before)
        {
            string after = before.Replace('.', 'a').Replace('@', 'a');
            for (int i = after.Length; i % 4 != 0 || i < 16; i++)
            {
                after += 'a';
            }
            return after;
        }

        public static string Encrypt(string unencrypted, string key, string vector)
        {
            var bvector = Convert.FromBase64String(prepKeyVector(vector)).Take(16).Select(i => i).ToArray();
            var cryptogram = bvector.Concat(Encrypt(encoder.GetBytes(unencrypted), Convert.FromBase64String(prepKeyVector(key)), bvector));
            return Convert.ToBase64String(cryptogram.ToArray());
        }

        public static string Decrypt(string encrypted, string key, string vector)
        {
            var cryptogram = Convert.FromBase64String(encrypted);
            if (cryptogram.Length < 17)
            {
                throw new ArgumentException("Not a valid encrypted string", "encrypted");
            }

            var bvector = Convert.FromBase64String(prepKeyVector(vector)).Take(16).Select(i => i).ToArray();
            var buffer = cryptogram.Skip(16).ToArray();
            return encoder.GetString(Decrypt(buffer, Convert.FromBase64String(prepKeyVector(key)), bvector));
        }

        private static byte[] Encrypt(byte[] buffer, byte[] key, byte[] vector)
        {
            var encryptor = rm.CreateEncryptor(key, vector);
            return Transform(buffer, encryptor);
        }

        private static byte[] Decrypt(byte[] buffer, byte[] key, byte[] vector)
        {
            var decryptor = rm.CreateDecryptor(key, vector);
            return Transform(buffer, decryptor);
        }

        private static byte[] Transform(byte[] buffer, ICryptoTransform transform)
        {
            var stream = new MemoryStream();
            using (var cs = new CryptoStream(stream, transform, CryptoStreamMode.Write))
            {
                cs.Write(buffer, 0, buffer.Length);
            }

            return stream.ToArray();
        }
    }
}