﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockBarang.Helper
{
    public static class TokopediaHelper
    {
        public static string FetchStoreName(string storeurl, string username, string password)
        {
            using (ChromeDriver driver = new ChromeDriver())
            {
                driver.Url = "https://www.tokopedia.com/login";
                driver.Navigate();
                driver.FindElement(By.Id("inputEmail")).SendKeys(username);
                driver.FindElement(By.Id("inputPassword")).SendKeys(password);
                driver.FindElement(By.Id("email_btn")).Click();
                if (driver.FindElement(By.ClassName("profile-completion__verified-sign")).GetAttribute("innerText").Trim().Length == 0)
                    throw new Exception("Failed Login");
                driver.Url = storeurl;
                driver.Navigate();
                var storename = driver.FindElement(By.XPath("//span[@itemprop='name']")).Text;
                driver.Url = "https://www.tokopedia.com/logout";
                driver.Navigate();
                driver.Close();
                return storename;
            }
        }
        public static void ChangeStock(string Url, Store store, string stock)
        {
            using (ChromeDriver driver = new ChromeDriver())
            {
                driver.Url = "https://www.tokopedia.com/login";
                driver.Navigate();
                driver.FindElement(By.Id("inputEmail")).SendKeys(store.Username);
                driver.FindElement(By.Id("inputPassword")).SendKeys(store.Password);
                driver.FindElement(By.Id("email_btn")).Click();
                if (driver.FindElement(By.ClassName("profile-completion__verified-sign")).GetAttribute("innerText").Trim().Length == 0)
                    throw new Exception("Failed Login");
                driver.Url = Url;
                driver.Navigate();
                driver.ExecuteScript("document.getElementById('invenage-value').setAttribute('value', '" + stock + "')");
                driver.ExecuteScript("$('button[value=save]').click();");
                driver.Url = "https://www.tokopedia.com/logout";
                driver.Navigate();
                driver.Close();
            }
        }
    }
}