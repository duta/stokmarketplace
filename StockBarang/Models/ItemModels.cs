﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using StockBarang.Helper;

namespace StockBarang.Models
{
    public class ItemCreateModel
    {
        [Required]
        [Display(Name = "Store")]
        public string Store { get; set; }
        public List<Store> StoreList { get; set; }

        [Required]
        [Display(Name = "Item")]
        public string ID { get; set; }
        public List<Material> MaterialList { get; set; }

        [Required]
        [Display(Name = "Item Url")]
        public string ItemUrl { get; set; }
    }

    public class ItemListModel : IPageable
    {
        public string Filter { get; set; }
        public List<Item> Items { get; set; }
        public List<PagingEntity> Paging { get; set; }
    }
    public class ItemStockModel
    {
        [Required]
        [Display(Name = "Stock")]
        public string stock { get; set; }
        public Item Item { get; set; }
    }
}