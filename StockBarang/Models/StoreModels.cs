﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using StockBarang.Helper;

namespace StockBarang.Models
{
    public class StoreCreateModel
    {
        [Required]
        [Display(Name = "Marketplace")]
        public string Marketplace { get; set; }

        [Required]
        [Display(Name = "Store URL")]
        public string StoreUrl { get; set; }

        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        public string UserId { get; set; }
    }

    public class StoreListModel : IPageable
    {
        public string Filter { get; set; }
        public List<Store> Stores { get; set; }
        public List<PagingEntity> Paging { get; set; }
    }
}