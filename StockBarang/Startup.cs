﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(StockBarang.Startup))]
namespace StockBarang
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
