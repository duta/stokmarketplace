﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockBarang.Helper
{
    public static class BukalapakHelper
    {
        public static string FetchStoreName(string storeurl, string username, string password)
        {
            ChromeDriverService service = ChromeDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = true;
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--headless");
            using (ChromeDriver driver = new ChromeDriver(service, options))
            {
                driver.Url = "https://www.bukalapak.com/login";
                driver.Navigate();
                driver.FindElement(By.Id("user_session_username")).SendKeys(username);
                driver.FindElement(By.Id("user_session_password")).SendKeys(password);
                driver.FindElement(By.XPath("//button[@name='commit']")).Click();
                driver.FindElement(By.ClassName("c-list-ui__link--username")).GetAttribute("innerText");
                driver.Url = storeurl;
                driver.Navigate();
                var storename = driver.FindElement(By.ClassName("user__name")).Text;
                driver.Url = "https://www.bukalapak.com/logout";
                driver.Navigate();
                driver.Close();
                return storename;
            }
        }

        public static void ChangeStock(string url, string username, string password, string stock)
        {
            ChromeDriverService service = ChromeDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = true;
            ChromeOptions options = new ChromeOptions();
            //options.AddArguments("--headless", "--disable-gpu", "--no-sandbox", "--incognito");
            options.AddArgument("--window-position=-32000,-32000");
            using (ChromeDriver driver = new ChromeDriver(service, options))
            {
                driver.Url = "https://www.bukalapak.com/login";
                driver.Navigate();
                if (driver.FindElements(By.ClassName("c-list-ui__link--username")).Count != 0)
                {
                    //last time failed to logout, we need to clean up
                    driver.Url = "https://www.bukalapak.com/logout";
                    driver.Navigate();
                    driver.Url = "https://www.bukalapak.com/login";
                    driver.Navigate();
                }
                driver.FindElement(By.Id("user_session_username")).SendKeys(username);
                driver.FindElement(By.Id("user_session_password")).SendKeys(password);
                driver.FindElement(By.XPath("//button[@name='commit']")).Click();
                if (driver.FindElements(By.ClassName("c-list-ui__link--username")).Count == 0)
                    throw new Exception("Failed Login");
                driver.Url = url;
                driver.Navigate();
                driver.ExecuteScript("document.getElementById('product_product_sku_stock').setAttribute('value', '" + stock + "')");
                driver.ExecuteScript("$('.btn--commit').click();");
                driver.Url = "https://www.bukalapak.com/logout"; 
                driver.Navigate();
                driver.Close();
            }
        }
    }
}