﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MadMilkman.Ini;
using System.IO;

namespace StockTask
{
    public partial class ControlForm : Form
    {
        public LoginForm login = null;
        public InfoForm info = null;
        internal AuthResponse ar = null;
        public static bool updating = false;
        private System.Threading.Timer timer;
        public IniFile iniapp;
        public IniFile iniuser;

        public ControlForm()
        {
            InitializeComponent();
            iniapp = new IniFile();
            if (File.Exists("StockTask.ini"))
            {
                iniapp.Load("StockTask.ini");
            }
            else
            {
                MessageBox.Show("Fatal error", "StockTask.ini tidak ditemukan");
            }
            iniuser = new IniFile();
            if (File.Exists(Path.Combine(Environment.GetFolderPath(
    Environment.SpecialFolder.ApplicationData), "StockTask.ini")))
            {
                iniuser.Load(Path.Combine(Environment.GetFolderPath(
    Environment.SpecialFolder.ApplicationData), "StockTask.ini"));
            }
            else
            {
                iniuser.Sections.Add("User");
                iniuser.Sections["User"].Keys.Add("token", "");
                iniuser.Sections["User"].Keys.Add("lastupdate", "");
                iniuser.Save(Path.Combine(Environment.GetFolderPath(
    Environment.SpecialFolder.ApplicationData), "StockTask.ini"));
            }
            this.Visible = false;
            if (string.IsNullOrEmpty(iniuser.Sections[0].Keys["token"].Value))
            {
                login = new LoginForm(this, AuthResponse.GetAutenticationURI(iniapp.Sections[0].Keys["client"].Value, ""));
                login.Show();
                toolStripMenuItemLogout.Visible = false;
                toolStripMenuItemLogin.Visible = true;
            }
            else
            {
                    notifyIcon1.ShowBalloonTip(1000, "Status", "Refresh Token found, fetching Auth Token", ToolTipIcon.Info);
                    info = new InfoForm(this);
                    ar = new AuthResponse()
                    {
                        refresh_token = iniuser.Sections[0].Keys["token"].Value,
                        clientId = iniapp.Sections[0].Keys["client"].Value,
                        secret = iniapp.Sections[0].Keys["secret"].Value
                    };
                    info.Refill(ar);
                    info.Show();
                    Task.Factory.StartNew(() =>
                            {
                                ar.refresh();
                                notifyIcon1.ShowBalloonTip(1000, "Status", "Login successful", ToolTipIcon.Info);
                                timer = new System.Threading.Timer((ev) =>
                                {
                                    Trigger("update");
                                }, null, TimeSpan.Zero, TimeSpan.FromSeconds(int.Parse(iniapp.Sections[0].Keys["interval"].Value)));
                            });
                }
        }

        internal void Login(string authCode)
        {
            Trigger("dispose");
            notifyIcon1.ShowBalloonTip(1000, "Status", "Refresh Token found, fetching Auth Token", ToolTipIcon.Info);
            info = new InfoForm(this);
            info.Refill(ar);
            info.Show();
            ar = AuthResponse.Exchange(authCode, iniapp.Sections[0].Keys["client"].Value, iniapp.Sections[0].Keys["secret"].Value, "urn:ietf:wg:oauth:2.0:oob");
            iniuser.Sections[0].Keys["token"].Value = ar.refresh_token;
            iniuser.Save(Path.Combine(Environment.GetFolderPath(
    Environment.SpecialFolder.ApplicationData), "StockTask.ini"));
            timer = new System.Threading.Timer((ev) =>
            {
                Trigger("update");
            }, null, TimeSpan.Zero, TimeSpan.FromMinutes(int.Parse(iniapp.Sections[0].Keys["interval"].Value)));
            notifyIcon1.ShowBalloonTip(1000, "Status", "Login successful", ToolTipIcon.Info);
        }

        internal void Trigger(string formname)
        {
            switch (formname)
            {
                case "dispose":
                    if (login != null)
                    {
                        login.Close();
                        login.Dispose();
                        login = null;
                    }
                    if (info != null)
                    {
                        info.Close();
                        info.Dispose();
                        info = null;
                    }
                    break;
                case "logout":
                    Trigger("dispose");
                    iniuser.Sections["User"].Keys.Add("token", "");
                    iniuser.Sections["User"].Keys.Add("lastupdate", "");
                    iniuser.Save(Path.Combine(Environment.GetFolderPath(
        Environment.SpecialFolder.ApplicationData), "StockTask.ini"));
                    timer.Dispose();
                    login = new LoginForm(this, new Uri("https://accounts.google.com/Logout?hl=en&continue="), true);
                    break;
                case "logoutnext":
                    login.Dispose();
                    login = new LoginForm(this, AuthResponse.GetAutenticationURI(iniapp.Sections[0].Keys["client"].Value, ""));
                    login.Show();
                    notifyIcon1.ShowBalloonTip(1000, "Status", "Logout successful, please login.", ToolTipIcon.Info);
                    break;
                case "login":
                    Trigger("dispose");
                    login = new LoginForm(this, AuthResponse.GetAutenticationURI(iniapp.Sections[0].Keys["client"].Value, ""));
                    login.Show();
                    break;
                case "update":
                    if (ControlForm.updating) break;
                    ControlForm.updating = true;
                    if (info != null) info.Invoke((MethodInvoker)delegate
                    {
                        // Running on the UI thread
                        info.Refill(ar);
                        toolStripMenuItemCheck.Enabled = false;
                        toolStripMenuItemCheck.Text = "Updating...";
                    });
                    Task.Factory.StartNew(() =>
                    {
                        var client = new HttpClient();
                        var request = new HttpRequestMessage()
                        {
                            RequestUri = new Uri(iniapp.Sections[0].Keys["urlfetch"].Value),
                            Method = HttpMethod.Get,
                        };
                        request.Headers.Add("authorization", "Bearer " + ar.Access_token);
                        client.SendAsync(request).ContinueWith(async (taskwithmsg) =>
                             {
                                 var response = taskwithmsg.Result;

                                 var content = await response.Content.ReadAsStringAsync();
                                 var json = JsonConvert.DeserializeObject<JsonStock[]>(content);
                                 var updated = new List<JsonStock>();
                                 foreach (var item in json)
                                 {
                                     try
                                     {
                                         if (item.marketplace == "Bukalapak")
                                         {
                                             StockBarang.Helper.BukalapakHelper.ChangeStock(
                                                 item.item_url, item.username, StockBarang.Helper.SimpleEncryptHelper.Decrypt(item.password, item.user_id, item.username), item.stock);
                                         }
                                         if (item.marketplace == "Tokopedia")
                                         {
                                             StockBarang.Helper.TokopediaHelper.ChangeStock(
                                                 item.item_url, item.username, StockBarang.Helper.SimpleEncryptHelper.Decrypt(item.password, item.user_id, item.username), item.stock);
                                         }
                                         updated.Add(item);
                                     }
                                     catch (Exception ex)
                                     {
                                         ex.ToString();
                                     }
                                 }
                                 client = new HttpClient();
                                 request = new HttpRequestMessage()
                                 {
                                     RequestUri = new Uri(iniapp.Sections[0].Keys["urlreport"].Value),
                                     Method = HttpMethod.Post,
                                 };
                                 request.Headers.Add("authorization", "Bearer " + ar.Access_token);
                                 request.Content = new StringContent(JsonConvert.SerializeObject(updated.ToArray()),
                                    Encoding.UTF8,
                                    "application/json");
                                 await client.SendAsync(request);
                                 iniuser.Sections["User"].Keys.Add("lastupdate", (DateTime.Now - DateTime.MinValue).TotalSeconds.ToString());
                                 iniuser.Save(Path.Combine(Environment.GetFolderPath(
                     Environment.SpecialFolder.ApplicationData), "StockTask.ini"));
                                 ControlForm.updating = false;
                                 notifyIcon1.ShowBalloonTip(1000, "Update", "Updated " + updated.Count + "/" + json.Length + " items.", ToolTipIcon.Info);
                             });
                    });
                    break;
                default:
                    break;
            }
        }

        private void toolStripMenuItemLogout_Click(object sender, EventArgs e)
        {
            Trigger("logout");
        }

        private void toolStripMenuItemQuit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void toolStripMenuItemInfo_Click(object sender, EventArgs e)
        {
            if (login != null)
            {
                this.login.Close();
                this.login.Dispose();
                login = null;
            }
            info = new InfoForm(this);
            info.Refill(ar);
            info.Show();
        }

        private void toolStripMenuItemCheck_Click(object sender, EventArgs e)
        {
            Trigger("update");
        }

        private void toolStripMenuItemLogin_Click(object sender, EventArgs e)
        {
            Trigger("dispose");
            login = new LoginForm(this, AuthResponse.GetAutenticationURI(iniapp.Sections[0].Keys["client"].Value, ""));
            login.Show();
        }

        private void notifyIcon1_BalloonTipShown(object sender, EventArgs e)
        {
            toolStripMenuItemLogout.Visible = ar != null;
            toolStripMenuItemLogin.Visible = ar == null;
            toolStripMenuItemLogout.Enabled = !(ar != null && ar.access_token == null);
            toolStripMenuItemLogin.Enabled = !(ar != null && ar.access_token == null);
            toolStripMenuItemCheck.Enabled = ar != null && ar.access_token != null;
            toolStripMenuItemCheck.Text = "Check Now";
            if (info != null) info.Refill(ar);
        }
    }

    public class JsonStock
    {
        public string iditem;
        public string user_id;
        public string username;
        public string password;
        public string marketplace;
        public string item_url;
        public string store_url;
        public string stock;
    }
}
