﻿using IWshRuntimeLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StockTask
{
    public partial class InfoForm : Form
    {
        private ControlForm master;
        public InfoForm(ControlForm master)
        {
            InitializeComponent();
            this.master = master;
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonLog_Click(object sender, EventArgs e)
        {
            if (buttonLogout.Text == "Logout")
                master.Trigger("logout");
            else
                master.Trigger("login");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (button3.Text == "Enable")
            {
                CreateLink();
            } else
            {
                DestroyLink();
            }
            Refill(master.ar);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            master.Trigger("update");
        }

        internal void Refill(AuthResponse ar)
        {
            if (ar != null && ar.access_token != null)
            {
                label1.Text = "Logged In";
                label1.ForeColor = Color.Green;
                button2.Enabled = !ControlForm.updating;
                if (ControlForm.updating) button2.Text = "Updating...";
                else button2.Text = "Check Now";
                buttonLogout.Text = "Logout";
            } else
            {
                label1.Text = "Not Logged In";
                label1.ForeColor = Color.Red;
                button2.Enabled = false;
                buttonLogout.Text = "Login";
            }
            buttonLogout.Enabled = !(ar != null && ar.access_token == null);
            if (!OnStartup())
            {
                label3.Text = "Start application on startup : Disabled";
                button3.Text = "Enable";
            } else
            {
                label3.Text = "Start application on startup : Enabled";
                button3.Text = "Disable";
            }
            if (string.IsNullOrEmpty(master.iniuser.Sections["User"].Keys["lastupdate"].Value))
            {
                label2.Text = "Last Update : N/A";
            } else
            {
                label2.Text = "Last Update : "+ DateTime.MinValue.AddSeconds(double.Parse(master.iniuser.Sections["User"].Keys["lastupdate"].Value)).ToString();
            }
        }

        public bool OnStartup()
        {
            string startUpFolderPath =
              Environment.GetFolderPath(Environment.SpecialFolder.Startup);
            return System.IO.File.Exists(startUpFolderPath + "\\" +
                Application.ProductName + ".lnk");
        }

        private void CreateLink()
        {
            WshShell wshShell = new WshShell();
            IWshShortcut shortcut;
            string startUpFolderPath =
              Environment.GetFolderPath(Environment.SpecialFolder.Startup);

            // Create the shortcut
            shortcut =
              (IWshShortcut)wshShell.CreateShortcut(
                startUpFolderPath + "\\" +
                Application.ProductName + ".lnk");

            shortcut.TargetPath = Application.ExecutablePath;
            shortcut.WorkingDirectory = Application.StartupPath;
            shortcut.Description = "Stock Task";
            // shortcut.IconLocation = Application.StartupPath + @"\App.ico";
            shortcut.Save();
        }

        private void DestroyLink()
        {
            string startUpFolderPath =
              Environment.GetFolderPath(Environment.SpecialFolder.Startup);
            System.IO.File.Delete(startUpFolderPath + "\\" +
                Application.ProductName + ".lnk");
        }

        private void InfoForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                master.Trigger("dispose");
            }
        }
    }
}
