﻿using System;
using System.Windows.Forms;

namespace StockTask
{
    public partial class LoginForm : Form
    {
        private ControlForm master;
        private bool destroyWhenComplete;

        public LoginForm(ControlForm master, Uri url, bool destroyWhenComplete = false)
        {
            InitializeComponent();
            webBrowser1.Navigate(url);
            this.master = master;
            this.destroyWhenComplete = destroyWhenComplete;
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (destroyWhenComplete)
            {
                master.Trigger("logoutnext");
                return;
            }
            string Mytitle = ((WebBrowser)sender).DocumentTitle.ToLower();
            if (Mytitle.IndexOf("success code=") > -1)
            {
                // searching the body for our code
                string authCode = webBrowser1.DocumentTitle.Replace("Success code=", "");
                authCode = authCode.Substring(0, authCode.IndexOf('&'));
                master.Login(authCode);
            }
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                master.Trigger("dispose");
            }
        }
    }
}
