﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StockBarang.Helper
{
    public static class TokopediaHelper
    {
        public static string FetchStoreName(string storeurl, string username, string password)
        {
            ChromeDriverService service = ChromeDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = true;
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--headless");
            using (ChromeDriver driver = new ChromeDriver(service, options))
            {
                driver.Url = "https://www.tokopedia.com/login";
                driver.Navigate();
                driver.FindElement(By.Id("inputEmail")).SendKeys(username);
                driver.FindElement(By.Id("inputPassword")).SendKeys(password);
                driver.FindElement(By.Id("email_btn")).Click();
                if (driver.FindElement(By.ClassName("profile-completion__verified-sign")).GetAttribute("innerText").Trim().Length == 0)
                    throw new Exception("Failed Login");
                driver.Url = storeurl;
                driver.Navigate();
                var storename = driver.FindElement(By.XPath("//span[@itemprop='name']")).Text;
                driver.Url = "https://www.tokopedia.com/logout";
                driver.Navigate();
                driver.Close();
                return storename;
            }
        }
        public static void ChangeStock(string Url, string username, string password, string stock)
        {
            ChromeDriverService service = ChromeDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = true;
            ChromeOptions options = new ChromeOptions();
            //options.AddArguments("--headless", "--disable-gpu", "--no-sandbox", "--incognito");
            options.AddArgument("--window-position=-32000,-32000");
            using (ChromeDriver driver = new ChromeDriver(service, options))
            {
                driver.Url = "https://www.tokopedia.com/login";
                driver.Navigate();
                if (driver.FindElements(By.ClassName("profile-completion__verified-sign")).Count != 0)
                {
                    //last time failed to logout, we need to clean up
                    driver.Url = "https://www.tokopedia.com/logout";
                    driver.Navigate();
                    driver.Url = "https://www.tokopedia.com/login";
                    driver.Navigate();
                }
                driver.FindElement(By.Id("inputEmail")).SendKeys(username);
                driver.FindElement(By.Id("inputPassword")).SendKeys(password);
                driver.FindElement(By.Id("email_btn")).Click();
                if (driver.FindElement(By.ClassName("profile-completion__verified-sign")).GetAttribute("innerText").Trim().Length == 0)
                    throw new Exception("Failed Login");
                driver.Url = Url;
                driver.Navigate();
                driver.ExecuteScript("document.getElementById('invenage-value').setAttribute('value', '" + stock + "')");
                driver.ExecuteScript("$('button[value=save]').click();");
                driver.Url = "https://www.tokopedia.com/logout";
                driver.Navigate();
                driver.Close();
            }
        }
    }
}